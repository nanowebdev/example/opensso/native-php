<?php 
    session_start();
    if(isset($_SESSION['token'])) {
        session_unset(); // remove all session variables
        session_destroy(); // destroy the session
        header('Location: index.php'); // go back to index page
    }
?>