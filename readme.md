# Native PHP
This is a simple example to integrate OpenSSO at Server Side with native PHP.

### Concept
The goals is to make users can login via OpenSSO login page in native PHP.  
So the flow is:

1. User trying to login via opensso
2. Token send back to callback page
3. Save the token to session so we can use the token to every page that require the credentials.
4. When the token has expired, users logout automatically.
5. Done.

Why we use session?  
Because using session is the simpler way. For production, better to use redis than session.

### Usage
To use this example script, make sure you have successfully running your OpenSSO.

### Installation

1. Run composer
```
composer install
```
2. Edit the `index.php` at line 3.  
```php
// CHANGE WITH YOUR OPENSSO URL
$url_sso = "http://localhost:3000/sso/login/3b09945411c1431da21f16dfd0002beb";
```

3. Replace `public.key` and `private.key` from your OpenSSO.  
4. Done.


### How to get URL SSO
1. Login to your OpenSSO
2. Go to menu `My SSO`.
3. Add your new SSO.  
![](https://gitlab.com/nanowebdev/example/opensso/native-php/-/raw/master/img/tutorial-1.png)
4. Now you have `URL SSO` login page.
![](https://gitlab.com/nanowebdev/example/opensso/native-php/-/raw/master/img/tutorial-2.png)
5. Done.

### Cheatsheet

1. Encode JWT
```php
encodeJWT($payload, $privateKey);
```

2. Decode JWT
```php
decodeJWT($token, $publicKey);
```

3. Validate JWT
```php
validateJWT($token, $publicKey);
```

This script is using [firebase/php-jwt](https://github.com/firebase/php-jwt).


### Need Help
Just chat with me via Telegram >> [https://t.me/aalfiann](https://t.me/aalfiann).
