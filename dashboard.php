<?php
    // Load the library
    require_once("./jwt.php");
    // start session
    session_start();
    
    $jwt = '';

    if(!isset($_SESSION["token"])) {
        // if token not found then reject
        header('Location: index.php');
    } else {
        // if token found then validate the token
        // load the public key
        $publicKey = file_get_contents('public.key');
        if(!validateJWT($_SESSION["token"], $publicKey)) {
            // if token validate expired or broken, we should destroy the token session and then reject
            session_unset(); // remove all session variables
            session_destroy(); // destroy the session

            // reject
            header('Location: index.php');
        } else {
            $jwt = json_decode(json_encode(decodeJWT($_SESSION["token"], $publicKey)));
        }
    }
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Signin Using OpenSSO Login Page</title>
        
        <!-- Bootstrap core CSS -->
        <link href="./css/bootstrap.min.css" rel="stylesheet">

        <!-- Example CSS -->
        <link href="./css/example.css" rel="stylesheet">
    </head>
    <body class="text-center">
        <div id="user-info" class="form-signin">
            <table class="table">
                <tbody class="text-left">
                    <tr>
                        <th scope="row" colspan="2"><img id="picture" src="<?php echo $jwt->gravatar?>"></th>
                    </tr>
                    <tr>
                        <th scope="row">User ID</th>
                        <td><span><?php echo $jwt->uid?></span></td>
                    </tr>
                    <tr>
                        <th scope="row">Username</th>
                        <td><span><?php echo $jwt->unm?></span></td>
                    </tr>
                    <tr>
                        <th scope="row">Name</th>
                        <td><span><?php echo $jwt->name?></span></td>
                    </tr>
                    <tr>
                        <th scope="row">Email</th>
                        <td><span><?php echo $jwt->mail?></span></td>
                    </tr>
                </tbody>
            </table>
            <button type="button" class="btn btn-danger" onclick="handleLogout()">Logout</button>
        </div>
        <script>
            function handleLogout() {
                location.href="logout.php";
            }
        </script>
    </body>
</html>