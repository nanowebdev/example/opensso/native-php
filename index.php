<?php 
    // CHANGE WITH YOUR OPENSSO URL
    $url_sso = "http://localhost:3000/sso/login/3b09945411c1431da21f16dfd0002beb";
    
    // Load the library
    require_once("./jwt.php");
    // start session
    session_start();
    if(isset($_SESSION["token"])) {
        $publicKey = file_get_contents('public.key');
        if(!validateJWT($_SESSION["token"], $publicKey)) {
            // if token validate expired or broken, we should destroy the token session
            session_unset(); // remove all session variables
            session_destroy(); // destroy the session
        } else {
            // if token still valid then redirect to dashboard
            header('Location: dashboard.php');
        }
    }
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Signin Using OpenSSO Login Page</title>
        
        <!-- Bootstrap core CSS -->
        <link href="./css/bootstrap.min.css" rel="stylesheet">

        <!-- Example CSS -->
        <link href="./css/example.css" rel="stylesheet">
    </head>
  <body class="text-center">
    
    <div id="login" class="form-signin">
        <h1 class="h3 mb-3 fw-normal">Sign in</h1>
        <hr>
        <!-- YOU MUST CHANGE THE URL LOGIN WITH YOUR OPENSSO -->
        <a href="<?php echo $url_sso?>" class="w-100 btn btn-lg btn-primary">Single Sign On</a>
    </div>
    </body>
</html>
