<?php

// Load the library from composer
require_once(__DIR__ . "/vendor/autoload.php");

use Firebase\JWT\JWT;
use Firebase\JWT\Key;

/**
 * Encode JWT
 * 
 * @param payload {array}
 * @param privateKey {string}
 * @return string  
 */
function encodeJWT($payload, $privateKey) {
    $jwt = JWT::encode($payload, $privateKey, 'RS256');
    return $jwt;
}

/**
 * Decode JWT
 * 
 * @param token {string}
 * @param publicKey {string}
 * @return array
 */
function decodeJWT($token, $publicKey) {
    try {
        JWT::$leeway = 60; // $leeway in seconds
        $decoded = JWT::decode($token, new Key($publicKey, 'RS256'));
        $decoded_array = (array) $decoded;
        return $decoded_array;
    } catch(Exception $e) {
        return [];
    }
}

/**
 * Validate JWT
 * 
 * @param token {string}
 * @param publicKey {string}
 * @return bool
 */
function validateJWT($token, $publicKey) {
    try {
        return decodeJWT($token, $publicKey);
    } catch (Exception $e) {
        return false;
    }
}

?>