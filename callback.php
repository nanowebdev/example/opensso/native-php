<?php
  // we will save the token into session.
  if($_GET['token']) {
    session_start();
    $token = $_GET['token'];
    $_SESSION["token"] = $token;
    if(isset($_SESSION["token"])) {
        header('Location: dashboard.php');
    }
  } else {
    echo 'Forbidden';
  }  
?>